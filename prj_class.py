from sqlalchemy import create_engine ,MetaData, Table, Column, Integer, String,NVARCHAR,DATE,and_,text,DateTime
from datetime import datetime 
import pyodbc
import sqlalchemy as sal
import pandas as pd
from sqlalchemy.sql import select

#ALPHA-PC\SQLDEV

class databBaseCRUD():
    def __init__(self):
        self.engine = sal.create_engine('mssql+pyodbc://ALPHA-PC\SQLDEV/projectDB?driver=SQL Server?Trusted_Connection=yes')
        self.connect=self.engine.connect()
        self.meta=MetaData()
        self.CUSTOMER=Table(
            'CUSTOMER',self.meta,
            Column('cid',Integer,primary_key=True),
            Column('cname',NVARCHAR)
        )
        self.ORDER1=Table(
            'ORDER1',self.meta,
            Column('oid',Integer,primary_key=True),
            Column('oDate',DateTime),
            Column('cid',Integer)
        )
        self.ORDERITEM=Table(
            'ORDERITEM',self.meta,
            Column('iid',Integer,primary_key=True),
            Column('oid',Integer),
            Column('qty',Integer),
            Column('pid',Integer)
        )
        self.PRODUCT=Table(
            'PRODUCT',self.meta,
            Column('pid',Integer),
            Column('pname',NVARCHAR),
            Column('price',Integer)
        )
    
    
        
    def getAllCustomers(self):
        ins=select([self.CUSTOMER])
        res=self.connect.execute(ins)
        print('getting all customers')
        for row in res:
            print(row)

    def getAllOrders(self):
        ins=self.ORDER1.select()
        res=self.connect.execute(ins)
        print('getting all orders')
        for row in res:
            print(row)

    def getAllProducts(self):
        ins=self.PRODUCT.select()
        res=self.connect.execute(ins)
        print('getting all products')
        for row in res:
            print(row)

    def getAllOrderItems(self):
        ins=self.ORDERITEM.select()
        res=self.connect.execute(ins)
        print('getting all orderItems')
        for row in res:
            print(row)

    def insertProduct(self):
        print('add a product')
        prname=input('enter product name::')
        prc=input('enter product price::')
        ins=self.PRODUCT.insert().values(pname=prname,price=prc)
        res=self.connect.execute(ins)

    def isnertCustomer(self):
        print('to insert a new Customer just print a name:')
        name=input()
        ins=self.CUSTOMER.insert().values(cname=name)
        res=self.connect.execute(ins)

    def getCustomerId(self):
        customerName=input('enter customer name::')
        ins=select([text("cid from CUSTOMER")]).where(text('CUSTOMER.cname=:x'))
        res=self.connect.execute(ins,x=customerName)
        for row in res:
            customerId=row[0]
            print('the customer id is::'+str(customerId))
        return customerId    
        


    def insertOrder(self):
        customerId=self.getCustomerId()
        print('choose one of the products to order')
        print('print the name and id of product')
        self.getAllProducts()
        productId=input('enter product ID::')
        productNum=input('enter number of the products you want::')
        buyTime=datetime.now()
        ins=self.ORDER1.insert().values(oDate=buyTime,cid=customerId)
        res=self.connect.execute(ins)
        ins=select([text("oid from ORDER1")]).where(and_(text('ORDER1.oDate=:x'),text('ORDER1.cid=:y')))
        res=self.connect.execute(ins,x=buyTime,y=customerId)
        """to get oid"""
        for row in res:
            orderId=row[0]
            print(row[0])
        ins=self.ORDERITEM.insert().values(oid=orderId,qty=productNum,pid=productId)
        res=self.connect.execute(ins)


    def getCutomerById(self):
        CID=input("Insert CustomerId to see customer::")
        ins=self.CUSTOMER.select().where(self.CUSTOMER.c.cid==CID)
        res=self.connect.execute(ins)
        for row in res:
            print(row)

    def getProductById(self):
        PID=input("Insert ProductId to see product::")
        ins=self.PRODUCT.select().where(self.PRODUCT.c.pid==PID)
        res=self.connect.execute(ins)
        for row in res:
            print(row)

    def getOrderItemById(self):
        IID=input("Insert orderItemId to see OrederItem::")
        ins=self.ORDERITEM.select().where(self.ORDERITEM.c.iid==IID)
        res=self.connect.execute(ins)
        for row in res:
            print(row)
    
    def getOrderById(self):
        OID=input("Insert orderId to see ORDER::")
        ins=self.ORDER1.select().where(self.ORDER1.c.oid==OID)
        res=self.connect.execute(ins)
        for row in res:
            print(row)

    def getCutomerOrders(self):
        customerId=self.getCustomerId()
        ins=self.ORDER1.select().where(self.ORDER1.c.cid==customerId)
        res=self.connect.execute(ins)
        print('getting all orders of the inseterd customer')
        for row in res:
            print(row)
        return res

    def getRelatedOrderItemsOfOrders(self):
        ins=self.ORDERITEM.select().where(self.ORDERITEM.c.oid==self.ORDER1.c.oid)
        res=self.connect.execute(ins)
        print('getting all ordersItems of the orders::')
        for row in res:
            print(row)
        print('\n')

    def getRelatedProductsOfOrderItems(self):
        ins=self.PRODUCT.select().where(self.PRODUCT.c.pid==self.ORDERITEM.c.pid)
        res=self.connect.execute(ins)
        print('getting all products of the orderItems::')
        for row in res:
            print(row)
        print('\n')

    def getProductOfEachOrderItemByOrderItemId(self):
        iiid=input('enter orderItemId to see related product::')
        ins=select([text('PRODUCT.pid,PRODUCT.pname,PRODUCT.price from PRODUCT,ORDERITEM')]).where(text('PRODUCT.pid=ORDERITEM.pid and ORDERITEM.iid=:x'))
        res=self.connect.execute(ins,x=iiid)
        print('related result:: ')
        for row in res:
            print(row)
        print('\n')
    
    def updateCustomer(self):
        custId=input('enter customerId to update the name:: ')
        newName=input('enter new name for customer:: ')
        ins=self.CUSTOMER.update().where(self.CUSTOMER.c.cid==custId).values(cname=newName)
        res=self.connect.execute(ins)

    def updateOrder(self):
        OID=input('enter orderItemID to update:: ')
        newCid=input('enter new customerId for this row:: ')
        newDate=input('enter new date for this row:: ')
        ins=self.ORDER1.update().where(self.ORDER1.c.oid==OID).values(cid=newCid,oDate=newDate)
        res=self.connect.execute(ins)
    
    def updateOrderItem(self):
        IID=input("enter orderitemID to upadte this row:: ")
        newQyt=input("enter new number of product:: ")
        newPid=input("enter new pid for this row:: ")
        ins=self.ORDERITEM.update().where(self.ORDERITEM.c.iid==IID).values(qty=newQyt,pid=newPid)
        res=self.connect.execute(ins)

    def upadteProduct(self):
        PID=input("enter productID to update:: ")
        newPrice=input("enter new price to update:: ")
        ins=self.PRODUCT.update().where(self.PRODUCT.c.pid==PID).values(price=newPrice)
        res=self.connect.execute(ins)

    def deleteCustomer(self):
        CID=input("enter customerID to delete:: ")
        ins=self.CUSTOMER.delete().where(self.CUSTOMER.c.cid==CID)
        res=self.connect.execute(ins)

    def delteProduct(self):
        PID=input("enter productID to delete:: ")
        ins=self.PRODUCT.delete().where(self.PRODUCT.c.pid==PID)
        res=self.connect.execute(ins)
    
    def deleteOrder(self):
        OID=input("enter orderID to delete:: ")
        ins=self.ORDER1.delete().where(self.ORDER1.c.oid==OID)
        res=self.connect.execute(ins)

    def deleteOrederItem(self):
        IID=input("enter orderItemID to delete:: ")
        ins=self.ORDERITEM.delete().where(self.ORDERITEM.c.iid==IID)
        res=self.connect.execute(ins)
